import {Component, OnInit} from '@angular/core';
import {Status} from '../model/Status';
import {Week} from '../model/Week';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  status: Status[];
  branchA: Week[] = [];
  branchB: Week[] = [];

  constructor() {
    this.status = [
      {used: this.randomData(31), available: 30},
      {used: this.randomData(36), available: 35},
      {used: this.randomData(26), available: 25}
    ];
    // generate data for branch A and branch B
    for (let i = 1; i <= 4; i++) {
      this.branchA.push(this.generateWeekRow(i));
      this.branchB.push(this.generateWeekRow(i));
    }

  }

  ngOnInit(): void {

  }

  randomData(value): number {
    return Math.floor(Math.random() * value);
  }

  generateWeekRow(weekDay): Week {
    return {
      name: 'week ' + weekDay,
      regular: {used: this.randomData(31), available: 30},
      diesel: {used: this.randomData(36), available: 35},
      premium: {used: this.randomData(25), available: 25}
    };
  }
}
