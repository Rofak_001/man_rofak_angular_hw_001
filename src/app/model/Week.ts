import {Status} from './Status';

export interface Week {
  name: string;
  regular: Status;
  diesel: Status;
  premium: Status;
}
