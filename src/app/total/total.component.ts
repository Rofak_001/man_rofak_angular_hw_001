import {Component, Input, OnInit} from '@angular/core';
import {Status} from '../model/Status';

@Component({
  selector: 'app-total',
  templateUrl: './total.component.html',
  styleUrls: ['./total.component.css']
})
export class TotalComponent implements OnInit {
  @Input() status: Status;
   isRed = false;
   isGreen = false;


  constructor() {
  }

  calculatePercent(): number {
    return this.status.used / this.status.available;
  }

  ngOnInit(): void {
    if (this.status.used / this.status.available * 100 > 70) {
      this.isRed = true;
      this.isGreen = false;
    } else {
      this.isRed = false;
      this.isGreen = true;
    }
  }

}
