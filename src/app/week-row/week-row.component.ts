import {Component, Input, OnInit} from '@angular/core';
import {Week} from '../model/Week';

@Component({
  // tslint:disable-next-line:component-selector
  selector: '[app-week-row]',
  templateUrl: './week-row.component.html',
  styleUrls: ['./week-row.component.css']
})
export class WeekRowComponent implements OnInit {
  @Input() row: Week;
  constructor() { }

  ngOnInit(): void {
  }

}
